Introduction
------------
_Wimakidown_ is a fork of [Pegdown v. 1.5.0](http://pegdown.org). But it rather doesn't use scala libraries so it's compatible with Java code.

Note
----
No features are planned but if you need something you can push your code here.

License
-------
_Wimakidown_ is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).


Patch Policy
------------
Anybody has access to pull and push changes. If some problems occure I will initiate some limiting push policy.

---
See also the [Wiki](https://bitbucket.org/stmare/wimakidown/wiki/Home)